CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

This module integrates the ultimate_cron configuration and log table
data with views. This allows views reports to provide details of which
cron jobs are taking the most time or running most frequently.

 * For a full description of the module visit:
   https://www.drupal.org/project/ultimate_cron_views

 * To submit bug reports and feature suggestions, or to track changes visit:
   https://www.drupal.org/project/issues/ultimate_cron_views


REQUIREMENTS
------------

This module requires the following modules.

 * 'Views' core module needs to be enabled.
 * 'Ultimate Cron' (https://www.drupal.org/project/ultimate_cron)


INSTALLATION
------------

Install the Ultimate Cron Views module as you would normally install a
contributed Drupal module. Visit https://www.drupal.org/node/1897420 for
further information.


CONFIGURATION
-------------

The module has no menu or modifiable settings. There is no configuration. When
enabled, the module will show cron jobs within a View under `Structure > Views
> Cron logs'.


MAINTAINERS
-----------

 * James Scott (jlscott) - https://www.drupal.org/u/jlscott
