<?php

namespace Drupal\ultimate_cron_views\Form;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Views;

/**
 * Class UltimateCronViewsSettingsForm.
 *
 * @ingroup ultimate_cron_views
 */
class UltimateCronViewsSettingsForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const MY_SETTINGS = 'ultimate_cron_views.settings';

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::MY_SETTINGS,
    ];
  }
  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'ultimate_cron_views_settings';
  }

  /**
   * Defines the settings form for user_history entities.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   Form definition array.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config(static::MY_SETTINGS);

    $message = ['Settings form for ultimate_cron_views.'];

    $form['ultimate_cron_views_settings']['#markup'] = implode('<br />', $message);

    // Return the values as a tree
    $form['#tree'] = TRUE;

    // Specify a fieldset for the data display fields
    $form['display'] = [
      '#type' => 'details',
      '#title' => 'Display settings',
      '#open' => TRUE,
    ];

    $format_options = [
      'custom' => $this->t('Custom'),
      'short' => $this->t('Default short date'),
      'medium' => $this->t('Default medium date'),
      'long' => $this->t('Default long date'),
      'fallback' => $this->t('Fallback date'),
      'html_datetime' => $this->t('HTML datetime'),
      'html_date' => $this->t('HTML date'),
      'html_time' => $this->t('HTML time'),
      'html_yearless_date' => $this->t('HTML yearless date'),
      'html_week' => $this->t('HTML week'),
      'html_month' => $this->t('HTML month'),
      'html_year' => $this->t('HTML year'),
    ];

    $form['display']['date_format'] = [
      '#type' => 'select',
      '#title' => $this->t('Date format'),
      '#description' => $this->t('Select the date format to use for views entries.'),
      '#options' => $format_options,
      '#default_value' => $config->get('display.date_format') ?: 'custom',
    ];

    $form['display']['custom_format'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Custom format'),
      '#description' => $this->t('If "custom" is selcted above, specify the PHP format string such as "Y-m-d H:i:s".'),
      '#default_value' => $config->get('display.custom_format') ?: 'Y-m-d H:i:s',
    ];

    // Specify a fieldset for the summary view display fields
    $form['summary'] = [
      '#type' => 'details',
      '#title' => 'Summary view',
      '#open' => FALSE,
    ];
    $form['summary']['view_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Summary view'),
      '#description' => $this->t('Specify the name of the view containing a summary display of the cron log data.'),
      '#default_value' => $config->get('summary.view_name') ?: 'ultimate_cron_views',
    ];
    $form['summary']['view_display'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Summary display'),
      '#description' => $this->t('Specify the name of the view display to show the summary cron log data.'),
      '#default_value' => $config->get('summary.view_display') ?: 'summary',
    ];

    // Specify a fieldset for the detail view display fields
    $form['detail'] = [
      '#type' => 'details',
      '#title' => 'Detail view',
      '#open' => FALSE,
    ];
    $form['detail']['view_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Detail view'),
      '#description' => $this->t('Specify the name of the view containing a Detail display of the cron log data.'),
      '#default_value' => $config->get('detail.view_name') ?: 'ultimate_cron_views',
    ];
    $form['detail']['view_display'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Detail display'),
      '#description' => $this->t('Specify the name of the view display to show the detailed cron log data.'),
      '#default_value' => $config->get('detail.view_display') ?: 'detail',
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * Form validation handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    $values = $form_state->getValues();

    // Check that the custom format is provided and is a valid format string is custom format chosen.
    if ($values['display']['date_format'] == 'custom') {
      $custom_format = trim($values['display']['custom_format']);
      try {
        $now = DrupalDateTime::createFromTimestamp(time());
        $date = $now->format($custom_format);
      }
      catch (\Exception $e) {
        $form_state->setErrorByName('display][custom_format', $this->t('Invalid custom date-time format string'));
      }
    }

    // Check that the specified views and displays are valid.
    if ($view = Views::getView($values['summary']['view_name'])) {
      if (!$view->setDisplay($values['summary']['view_display'])) {
        $form_state->setErrorByName('summary][view_display', $this->t('Invalid display for cron logs summary view.'));
      }
    }
    else {
      $form_state->setErrorByName('summary][view_name', $this->t('Invalid view name for cron logs summary.'));
    }

    if ($view = Views::getView($values['detail']['view_name'])) {
      if (!$view->setDisplay($values['detail']['view_display'])) {
        $form_state->setErrorByName('detail][view_display', $this->t('Invalid display for cron logs detail view.'));
      }
    }
    else {
      $form_state->setErrorByName('detail][view_name', $this->t('Invalid view name for cron logs detail.'));
    }

  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    // Retrieve the configuration.
    $config = $this->configFactory->getEditable(static::MY_SETTINGS);

    $values = $form_state->getValues();

    $config->set('display.date_format', $values['display']['date_format']);
    $config->set('display.custom_format', trim($values['display']['custom_format']));

    $config->set('summary.view_name', $values['summary']['view_name']);
    $config->set('summary.view_display', $values['summary']['view_display']);

    $config->set('detail.view_name', $values['detail']['view_name']);
    $config->set('detail.view_display', $values['detail']['view_display']);

    $config->save();

    // Invalidate the cached views
    $view_names = array_unique([$values['summary']['view_name'], $values['detail']['view_name']]);
    foreach ($view_names as $view_name) {
      $view = Views::getView($view_name);
      if ($view) {
        Cache::invalidateTags($view->getCacheTags());
      }
    }

    parent::submitForm($form, $form_state);

  }

}
