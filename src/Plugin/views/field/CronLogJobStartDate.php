<?php


namespace Drupal\ultimate_cron_views\Plugin\views\field;


use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class CronLogJobStartDate
 *
 * @ViewsField("cron_log_job_start_date")
 */
class CronLogJobStartDate extends FieldPluginBase {

  /**
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * UltimateCronViewsController constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($configuration, $plugin_id, $plugin_definition, $container->get('config.factory'));
  }

  /**
   * Define the available options
   * @return array
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['precision'] = ['default' => 3];

    return $options;
  }

  /**
   * Provide the options form.
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {

    $options = [0, 1, 2, 3, 4];

    $form['precision'] = [
      '#title' => $this->t('Select required precision for start time seconds.'),
      '#type' => 'select',
      '#default_value' => $this->options['precision'],
      '#options' => $options,
    ];

    parent::buildOptionsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {

    $config = $this->configFactory->get('ultimate_cron_views.settings');
    $format_name = $config->get('display.date_format');
    $format_string = NULL;
    if ($format_name == 'custom') {
      $format_string = $config->get('display.custom_format');
    }

    $start_date = '';
    $precision = $this->options['precision'];

    if (isset($values->ultimate_cron_log_start_time)) {
      $start_time = number_format(round($values->ultimate_cron_log_start_time, $precision), $precision, '.', '');
      $start = explode('.', $start_time);
      $start_date = \Drupal::service('date.formatter')->format($start[0], $format_name, $format_string);
      $start_date .= '.' . $start[1];
    }

    return [
      '#markup' => $start_date,
    ];

  }

  /**
   * Called to add the real field to a query.
   */
  public function query() {
    $this->realField = 'start_time';
    parent::query();
  }

}
