<?php


namespace Drupal\ultimate_cron_views\Plugin\views\field;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * Class CronLogJobDuration
 *
 * @ViewsField("cron_log_job_duration")
 */
class CronLogJobDuration extends FieldPluginBase {

  /**
   * Define the available options
   * @return array
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['precision'] = ['default' => 3];

    return $options;
  }

  /**
   * Provide the options form.
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {

    $options = [0, 1, 2, 3, 4];

    $form['precision'] = [
      '#title' => $this->t('Select required precision for duration value.'),
      '#type' => 'select',
      '#default_value' => $this->options['precision'],
      '#options' => $options,
    ];

    parent::buildOptionsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {

    $duration = 0;
    $precision = $this->options['precision'];

    if (isset($values->ultimate_cron_log_start_time) && isset($values->ultimate_cron_log_end_time)) {
      $duration = $values->ultimate_cron_log_end_time - $values->ultimate_cron_log_start_time;
    }

    return [
      '#markup' => number_format(round($duration, $precision), $precision, '.', ''),
    ];

  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // This function exists to override parent query function.
    // Do nothing.
  }

}
