<?php


namespace Drupal\ultimate_cron_views\Plugin\views\query;


use Drupal\views\Plugin\views\query\Sql;


/**
 * Class CronLogSqlQuery
 *
 * Custom views query plugin for an SQL query.
 *
 * @ViewsQuery(
 *   id = "cron_log_views_query",
 *   title = @Translation("Cron Log Views Query"),
 *   help = @Translation("Query will be generated and run using the Drupal database API.")
 * )
 */
class CronLogSqlQuery extends Sql {

  /**
   * This function will translate the cacluated datetime field names into the
   * real database column names.
   *
   * @param string $snippet
   *
   * @return string
   */
  private function fieldToRealfield($snippet) {
    if (strpos($snippet, 'start_datetime') !== FALSE) {
      return str_replace('start_datetime', 'start_time', $snippet);
    }
    elseif (strpos($snippet, 'end_datetime') !== FALSE) {
      return str_replace('end_datetime', 'end_time', $snippet);
    }
    else {
      return $snippet;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function addWhere($group, $field, $value = NULL, $operator = NULL) {

    $field = $this->fieldToRealfield($field);
    parent::addWhere($group, $field, $value, $operator);
  }

  /**
   * {@inheritdoc}
   */
  public function addWhereExpression($group, $snippet, $args = []) {

    $snippet = $this->fieldToRealfield($snippet);
    parent::addWhereExpression($group, $snippet, $args);

  }

  /**
   * {@inheritdoc}
   */
  public function addHavingExpression($group, $snippet, $args = []) {

    $snippet = $this->fieldToRealfield($snippet);
    parent::addHavingExpression($group, $snippet, $args);

  }

  /**
   * {@inheritdoc}
   */
  public function addOrderBy($table, $field = NULL, $order = 'ASC', $alias = '', $params = []) {

    $field = $this->fieldToRealfield($field);
    parent::addOrderBy($table, $field, $order, $alias, $params);

  }

  /**
   * {@inheritdoc}
   */
  public function addGroupBy($clause) {

    $clause = $this->fieldToRealfield($clause);
    parent::addGroupBy($clause);

  }

    /**
   * {@inheritdoc}
   */
  public function addField($table, $field, $alias = '', $params = []) {

    switch ($field) {
      case 'start_datetime':
      case 'end_datetime':
        // Make sure an alias is assigned
        $alias = $alias ? $alias : $table . '_' . $field;
        $alias = strtolower(substr($alias, 0, 60));
        // Specify the real field
        $real_field = str_replace('date', '', $field);

        // Create a field info array.
        $field_info = [
            'field' => "FROM_UNIXTIME(ROUND($table.$real_field, 3), '%Y-%m-%d %H:%i:%s')",
            'alias' => $alias,
          ] + $params;

        // Test to see if the field is actually the same or not. Due to
        // differing parameters changing the aggregation function, we need
        // to do some automatic alias collision detection:
        $base = $alias;
        $counter = 0;
        while (!empty($this->fields[$alias]) && $this->fields[$alias] != $field_info) {
          $field_info['alias'] = $alias = $base . '_' . ++$counter;
        }

        if (empty($this->fields[$alias])) {
          $this->fields[$alias] = $field_info;
        }

        // Keep track of all aliases used.
        $this->fieldAliases[$table][$real_field] = $alias;

        return $alias;

      case 'duration':
        // Make sure an alias is assigned
        $alias = $alias ? $alias : $table . '_' . $field;
        $alias = strtolower(substr($alias, 0, 60));

        // Create a field info array.
        // TODO find a way to set the precision from a config setting.
        $field_info = [
            'field' => 'ROUND(' . $table . '.end_time - ' . $table . '.start_time, 3)',
            'alias' => $alias,
          ] + $params;

        // Test to see if the field is actually the same or not. Due to
        // differing parameters changing the aggregation function, we need
        // to do some automatic alias collision detection:
        $base = $alias;
        $counter = 0;
        while (!empty($this->fields[$alias]) && $this->fields[$alias] != $field_info) {
          $field_info['alias'] = $alias = $base . '_' . ++$counter;
        }

        if (empty($this->fields[$alias])) {
          $this->fields[$alias] = $field_info;
        }

        // Keep track of all aliases used.
        $this->fieldAliases[$table][$field] = $alias;

        return $alias;

      default:
        return parent::addField($table, $field, $alias, $params);

    }

  }

}
