<?php

namespace Drupal\ultimate_cron_views\Controller;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\views\Views;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Default controller for the ultimate_cron_views module.
 */
class UltimateCronViewsController extends ControllerBase {

  /**
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * @var \Drupal\Core\Messenger\MessengerInterface;
   */
  protected $messenger;

  /**
   * UltimateCronViewsController constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The Drupal messenger service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, MessengerInterface $messenger) {
    $this->configFactory = $config_factory;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('messenger')
    );
  }

  /**
   * Returns view of ultimate_cron log summary.
   *
   * @return mixed array|null
   *   The view response as a render array, or a warning message.
   */
  public function summary() {
    return $this->display('summary');
  }

  /**
   * Returns view of ultimate_cron log details.
   *
   * @return mixed array|null
   *   The view response as a render array or a warning message.
   */
  public function detail() {
    return $this->display('detail');
  }

  /**
   * Returns view of ultimate_cron log data.
   *
   * @param $display
   * @return array|null
   */
  public function display($display) {

    $config = $this->configFactory->get('ultimate_cron_views.settings');
    $view_name = $config->get($display . '.view_name');
    $view_display = $config->get($display . '.view_display');


    $view = Views::getView($view_name);
    if ($view) {
      if ($view->setDisplay($view_display)) {
        return $view->render();
      }
      else {
        // Missing detail display.
        $this->messenger->addWarning($this->t('Specified display "%display" of view %name is not available',
          ['%display' => $view_display, '%name' => $view_name])
        );
      }
    }
    else {
      // Missing default view.
      $this->messenger->addWarning($this->t('Specified view for ultimate_cron logs ("%name") is not available',
        ['%name' => $view_name])
      );
    }

    //return new RedirectResponse('admin/config/system/cron', '404');
    throw new NotFoundHttpException();

  }

}
