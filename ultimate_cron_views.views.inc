<?php

/**
 * Implements hook_views_data().
 */
function ultimate_cron_views_views_data() {

  $data = [];

  // Describe the db table ultimate_cron_log to views so the log entries can
  // be linked to ultimate_cron_jobs and used in views displays.

  // The outermost keys of $data are Views table names, which should usually
  // be the same as the hook_schema() table names.
  $data['ultimate_cron_log'] = [];
  // The value corresponding to key 'table' gives properties of the table
  // itself.
  $data['ultimate_cron_log']['table'] = [];
  // Within 'table', the value of 'group' (translated string) is used as a
  // prefix in Views UI for this table's fields, filters, etc. When adding
  // a field, filter, etc. you can also filter by the group.
  $data['ultimate_cron_log']['table']['group'] = t('Ultimate cron');
  // Within 'table', the value of 'provider' is the module that provides schema
  // or the entity type that causes the table to exist. Setting this ensures
  // that views have the correct dependencies. This is automatically set to the
  // module that implements hook_views_data().
  $data['ultimate_cron_log']['table']['provider'] = 'ultimate_cron';

  // Some tables are "base" tables, meaning that they can be the base tables
  // for views. Non-base tables can only be brought in via relationships in
  // views based on other tables. To define a table to be a base table, add
  // key 'base' to the 'table' array:
  $data['ultimate_cron_log']['table']['base'] = [
    'query_id' => 'cron_log_views_query', // This is the query plugin to use
    'field' => 'name', // This is the identifier field for the view.
    'title' => t('Ultimate cron logs'),
    'help' => t('Ultimate cron log entries related to cron jobs.'),
  ];

  $data['ultimate_cron_log']['lid'] = [
    'title' => t('LID'),
    'help' => t('Lock id'),
    'field' => [
      'id' => 'standard',
    ],
    'sort' => [
      // ID of sort handler plugin to use.
      'id' => 'standard',
    ],
    'filter' => [
      // ID of filter handler plugin to use.
      'id' => 'standard',
    ],
    'argument' => [
      // ID of argument handler plugin to use.
      'id' => 'standard',
    ],
  ];

  $data['ultimate_cron_log']['name'] = [
    'title' => t('Name'),
    'help' => t('The name of the cron job generating the log record'),
    'field' => [
      'id' => 'standard',
    ],
    'sort' => [
      // ID of sort handler plugin to use.
      'id' => 'standard',
    ],
    'filter' => [
      // ID of filter handler plugin to use.
      'id' => 'standard',
    ],
    'argument' => [
      // ID of argument handler plugin to use.
      'id' => 'standard',
    ],
  ];

  $data['ultimate_cron_log']['log_type'] = [
    'title' => t('Log type'),
    'help' => t('The Ultimate cron log type'),
    'field' => [
      'id' => 'numeric',
    ],
    'sort' => [
      // ID of sort handler plugin to use.
      'id' => 'standard',
    ],
    'filter' => [
      // ID of filter handler plugin to use.
      'id' => 'numeric',
    ],
    'argument' => [
      // ID of argument handler plugin to use.
      'id' => 'numeric',
    ],
  ];

  $data['ultimate_cron_log']['start_time'] = [
    'title' => t('Start time'),
    'help' => t('The start time of the Ultimate cron job'),
    'field' => [
      'id' => 'numeric',
      'float' => TRUE,
    ],
    'sort' => [
      // ID of sort handler plugin to use.
      'id' => 'standard',
    ],
    'filter' => [
      // ID of filter handler plugin to use.
      'id' => 'numeric',
    ],
    'argument' => [
      // ID of argument handler plugin to use.
      'id' => 'numeric',
    ],
  ];

  $data['ultimate_cron_log']['end_time'] = [
    'title' => t('End time'),
    'help' => t('The end time of the Ultimate cron job'),
    'field' => [
      'id' => 'numeric',
      'float' => TRUE,
    ],
    'sort' => [
      // ID of sort handler plugin to use.
      'id' => 'standard',
    ],
    'filter' => [
      // ID of filter handler plugin to use.
      'id' => 'numeric',
    ],
    'argument' => [
      // ID of argument handler plugin to use.
      'id' => 'numeric',
    ],
  ];

  $data['ultimate_cron_log']['uid'] = [
    'title' => t('Uid'),
    'help' => t('The user id associated with the Ultimate cron job'),
    'field' => [
      'id' => 'numeric',
    ],
    'sort' => [
      // ID of sort handler plugin to use.
      'id' => 'standard',
    ],
    'filter' => [
      // ID of filter handler plugin to use.
      'id' => 'numeric',
    ],
    'argument' => [
      // ID of argument handler plugin to use.
      'id' => 'numeric',
    ],
  ];

  $data['ultimate_cron_log']['init_message'] = [
    'title' => t('Initial message'),
    'help' => t('The initial message from the Ultimate cron job'),
    'field' => [
      'id' => 'standard',
    ],
    'sort' => [
      // ID of sort handler plugin to use.
      'id' => 'standard',
    ],
    'filter' => [
      // ID of filter handler plugin to use.
      'id' => 'standard',
    ],
    'argument' => [
      // ID of argument handler plugin to use.
      'id' => 'standard',
    ],
  ];

  $data['ultimate_cron_log']['message'] = [
    'title' => t('Message'),
    'help' => t('The message from the Ultimate cron job'),
    'field' => [
      'id' => 'standard',
    ],
    'sort' => [
      // ID of sort handler plugin to use.
      'id' => 'standard',
    ],
    'filter' => [
      // ID of filter handler plugin to use.
      'id' => 'standard',
    ],
    'argument' => [
      // ID of argument handler plugin to use.
      'id' => 'standard',
    ],
  ];

  $data['ultimate_cron_log']['severity'] = [
    'title' => t('Severity'),
    'help' => t('The severity code of the Ultimate cron job'),
    'field' => [
      'id' => 'numeric',
    ],
    'sort' => [
      // ID of sort handler plugin to use.
      'id' => 'standard',
    ],
    'filter' => [
      // ID of filter handler plugin to use.
      'id' => 'numeric',
    ],
    'argument' => [
      // ID of argument handler plugin to use.
      'id' => 'numeric',
    ],

  ];

  // Custom formatted field
  $data['ultimate_cron_log']['start_datetime'] = [
    'title' => t('Start date/time'),
    'help' => t('The start date/time of the Ultimate cron job'),
    'field' => [
      'id' => 'cron_log_job_start_date',
    ],
    'sort' => [
      // ID of sort handler plugin to use.
      'id' => 'date',
    ],
    'filter' => [
      // ID of filter handler plugin to use.
      'id' => 'date',
    ],
    'argument' => [
      // ID of argument handler plugin to use.
      'id' => 'date',
    ],

  ];

  // Custom formatted field
  $data['ultimate_cron_log']['end_datetime'] = [
    'title' => t('End date/time'),
    'help' => t('The end date/time of the Ultimate cron job'),
    'field' => [
      'id' => 'cron_log_job_end_date',
    ],
    'sort' => [
      // ID of sort handler plugin to use.
      'id' => 'date',
    ],
    'filter' => [
      // ID of filter handler plugin to use.
      'id' => 'date',
    ],
    'argument' => [
      // ID of argument handler plugin to use.
      'id' => 'date',
    ],

  ];

  // Custom caclulated field
  $data['ultimate_cron_log']['duration'] = [
    'title' => t('Duration'),
    'help' => t('The run time duration of the Ultimate cron job'),
    'field' => [
      'id' => 'cron_log_job_duration',
      'float => TRUE,'
    ],
    'sort' => [
      // ID of sort handler plugin to use.
      'id' => 'standard',
    ],
    'filter' => [
      // ID of filter handler plugin to use.
      'id' => 'numeric',
    ],
    'argument' => [
      // ID of argument handler plugin to use.
      'id' => 'numeric',
    ],

  ];

  return $data;
}

/**
 * Implements hook_views_data_alter().
 */
function ultimate_cron_views_views_data_alter(&$data) {

  if (isset($data['ultimate_cron_job'])) {

    // This table references the virtual ultimate_cron_job table.
    // This virtual table is defined as:
    //  $data['ultimate_cron_job'] => [
    //    'table' => [
    //      'group' => 'Cron Job',
    //      'provider' => 'ultimate_cron_job',
    //      'base' => [
    //        'field' => 'machine-readable name',
    //        'index' => 'machine-readable name',
    //        'title' => 'Cron job',
    //        'help' => '',
    //        'query_id' => 'config_view_query',
    //      ],
    //    ],
    //    'title' => [
    //      'title' => 'Title',
    //      'help' => 'Title',
    //      'field' => [...],
    //      'filter' => [...],
    //      'sort => [...],
    //      'argument' => [...],
    //    ],
    //    'id' => [...],
    //    'weight' => [...],
    //    'module' => [...],
    //    'callback' => [...],
    //  ];
    //
    // The declaration below creates an 'implicit' relationship to that table,
    // so that when it is the base table, the fields in ultimate_cron_log are
    // automatically available.
    $data['ultimate_cron_log']['table']['join'] = [
      // Index this array by the table name to which this table refers.
      // 'left_field' is the primary key in the referenced table.
      // 'field' is the foreign key in this table.
      'ultimate_cron_job' => [
        'left_field' => 'id',
        'field' => 'name',
      ],
    ];

    // Define a relationship to the virtual ultimate_cron_job table, so views whose
    // base table is ultimate_cron_job can add a relationship to log entries.
    //$data['ultimate_cron_log']['name']['relationship'] = [
    // Views name of the table to join to for the relationship.
    //  'base' => 'ultimate_cron_job',
    // Database field name in the other table to join on.
    //  'base field' => 'id',
    // ID of relationship handler plugin to use.
    //  'id' => 'standard',
    // Default label for relationship in the UI.
    //  'label' => t('Log entries'),
    //];

  }

}
